/*-
 * Copyright (c) 2010, Aldis Berjoza <aldis@bsdroot.lv>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following disclaimer
 *    in the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name of the  nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* BUGS:
 * Rotate should check for home
 * No paused
 * No drop
 * No Score
 * Bug when hiding full walls, some levels doesn't go down
 */


#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <GL/glu.h>
#include <GL/glut.h>
#include <png.h>

#include "msg.h"

typedef struct {
    int	    width;
    int	    height;
    char    *data;
} IMG;
IMG * img = NULL;

#define FIELD_W 10
#define FIELD_H 24
char field[FIELD_W][FIELD_H];
#define BLOCK_W 4
#define BLOCK_H 4
char block[BLOCK_W][BLOCK_H];

int blockType = 0;
int blockW = 0;
int blockH = 0;

int htop = 0;
int posy;
int posx;
int gameOver = 0;
long score = 0;
int timeout = 300;
int paused = 0;


GLint screen_width = 240;
GLint screen_height = 480;
GLfloat	aspect_ratio = 0;

IMG *load_img_from_png(const char * filename);
void FreeMem(void);
void checkHouse(void);
void clearBlock(void);
void clearFlour(int *flour);
void createBlock(void);
void display(void);
void displayGL(void);
void displayResizeGL(GLint screen_width, GLint screen_height);
void fixBlock(void);
void free_img(IMG **img);
void getBlockDimentions(void);
void kbd_funkc(unsigned char key, int x, int y);
void kbd_funkc(unsigned char key, int x, int y);
void kbd_specfunkc(unsigned char key, int x, int y);
void mainLoop(int Null);
void moveBlockDown(void);
void rotateBlock(int times);


#define BYTES_TO_TEST_PNG 4	/* No more than 8 */

/* int main(void) {{{1 */
int
main(int argc, char **argv)
{

    for (int y = 0; y < FIELD_H; y++)
	for (int x = 0; x < FIELD_W; x++)
	    field[x][y] = 0;
    printf("\x1B\c");

    img = load_img_from_png("block.png");
    atexit(FreeMem);

    sranddev();	    /* random seed */

    aspect_ratio = (GLfloat) screen_width / (GLfloat) screen_height;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(screen_width, screen_height);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("jetris - Jet Another teTRIS");
    createBlock();
    glutDisplayFunc(displayGL);
    glutTimerFunc(timeout, mainLoop, 0);
    glutReshapeFunc(displayResizeGL);
    glutSpecialFunc(kbd_specfunkc);
    glutKeyboardFunc(kbd_funkc);

    glOrtho (0, screen_width, screen_height, 0, 0, 1);
    glMatrixMode (GL_MODELVIEW);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_ALPHA_TEST);
    glClearColor(0,0,0,255);

    glutMainLoop();

    return EXIT_SUCCESS;
} /* 1}}} */

/* void FreeMem(void) {{{1 */
void 
FreeMem(void)
{
    free_img(&img);
} /* 1}}} */

/* IMG * load_img_from_png(const char * filename) {{{1 */
IMG *
load_img_from_png(const char * filename)
{
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    png_infop end_info = NULL;
    FILE *fp = NULL;
    unsigned char buff[BYTES_TO_TEST_PNG];

    if ((fp = fopen(filename, "rb")) == NULL)
	msg_die("Unable to open file\n");

    fread(buff, BYTES_TO_TEST_PNG, 1 , fp);
    if ((png_sig_cmp(buff, 0, BYTES_TO_TEST_PNG)) != 0)
	msg_die("Not png file\n");

    if ((png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)) == NULL)
	msg_die("Can't create png read struct\n");

    if ((info_ptr = png_create_info_struct(png_ptr)) == NULL)
	msg_die("Can't create png info struct\n");

    if ((end_info = png_create_info_struct(png_ptr)) == NULL)
	msg_die("Can't create png end info struct\n");

    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, BYTES_TO_TEST_PNG);
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

    fclose(fp);

    img = malloc(sizeof(IMG));
    img->width = png_get_image_width(png_ptr, info_ptr);
    img->height = png_get_image_height(png_ptr, info_ptr);
    img->data = calloc(img->width * img->height, 4); /* RGBA */

    /* Get IMAGE DATA. Comes in the form of an array of pointers to pixel rows. */
    png_bytep *row_pointers = png_get_rows(png_ptr, info_ptr);

    for (int y = 0; y < img->height; y++) {
	memcpy(img->data + y * img->width * 4, row_pointers[y], img->width * 4);
    }

    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);

    return img;
} /* 1}}} */

/* void free_img(IMG **img) {{{1 */
void
free_img(IMG **img)
{
    free((*img)->data);
    free(*img);
    (*img) = NULL;
} /* 1}}} */

/* void clearBlock(void) {{{1 */
void
clearBlock(void)
{
    for (int x = 0; x < BLOCK_W; x++) {
	for (int y = 0; y < BLOCK_H; y++) {
	    block[x][y] = 0;
	}
    }
} /* 1}}} */

/* void getBlockDimentions(void) {{{1 */
void
getBlockDimentions(void)
{
    blockW = 0;
    blockH = 0;
    for (int y = BLOCK_H - 1; y >= 0; y--) {
	for (int x = BLOCK_W - 1; x >= 0; x--) {
	    if (block[x][y] != 0) {
		if (blockW <= x)
		    blockW = x + 1;
		if (blockH <= y)
		    blockH = y + 1;
	    }
	}
    }
} /* 1}}} */

/* void rotateBlock(int times) {{{1 */
void
rotateBlock(int times)
{
    if (times != 0) {

	int tmp_block[BLOCK_W][BLOCK_H];

	/* Rotate */
	for (int i = 0; i < times; i++) {
	    for (int x = 0; x < BLOCK_W; x++) {
		for (int y = 0; y < BLOCK_H; y++) {
		    tmp_block[x][y] = block[y][BLOCK_W-1-x];
		}
	    }

	    for (int x = 0; x < BLOCK_W; x++) {
		for (int y = 0; y < BLOCK_H; y++) {
		    block[x][y] = tmp_block[x][y];
		}
	    }
	}

	/* normalize */
	int nx = BLOCK_W;
	int ny = BLOCK_H;

	for (int y = 0; y < BLOCK_H; y++) { /* Y */
	    for (int x = 0; x < BLOCK_W; x++) { /* X */
		if (block[x][y] != 0) {
		    if (nx > x)
			nx = x;
		    if (ny > y)
			ny = y;
		}
	    }
	}

	if (nx != 0 || ny != 0) {
	    if (nx != 0) {
		for (int x = 0; x < BLOCK_W - nx; x++) {
		    for (int y = 0; y < BLOCK_H; y++) {
			block[x][y] = block[x+nx][y];
			block[x+nx][y] = 0;
		    }
		}
	    }
	    if (ny != 0) {
		for (int y = 0; y < BLOCK_H - ny; y++) {
		    for (int x = 0; x < BLOCK_W; x++) {
			block[x][y] = block[x][y+ny];
			block[x][y+ny] = 0;
		    }
		}
	    }
	}
    }
    getBlockDimentions();
    if (posx + blockW > FIELD_W) 
	posx -= posx + blockW - FIELD_W;
} /* 1}}} */

/* void mainLoop(int Null) {{{1 */
void
mainLoop(int Null)
{
    if (gameOver == 0) {
	if (paused == 0) {
	    moveBlockDown();
	    display();
	    if (htop >= FIELD_H - BLOCK_H - 1) {
		msg_debug("GAME OVER\n");
		exit(0);
	    }
	    displayGL();
	}
	glutTimerFunc(timeout, mainLoop, 0);
    }

} /* 1}}} */

/* void createBlock(void) {{{1 */
void
createBlock(void)
{
    clearBlock();
    posy = FIELD_H - BLOCK_H;
    int block_type = (int) (6.0 / RAND_MAX * rand()) + 1;
    int block_rot = (int) (3.0 / RAND_MAX * rand());
    switch (block_type) {
    case 1:		    /* O */
	block[0][0] = 1;
	block[0][1] = 1;
	block[1][0] = 1;
	block[1][1] = 1;
	break;
    case 2:		    /* I */
	block[0][0] = 2;
	block[0][1] = 2;
	block[0][2] = 2;
	block[0][3] = 2;
	break;
    case 3:		    /* L */
	block[1][0] = 3;
	block[0][0] = 3;
	block[0][1] = 3;
	block[0][2] = 3;
	break;
    case 4:		    /* J */
	block[1][2] = 4;
	block[0][0] = 4;
	block[0][1] = 4;
	block[0][2] = 4;
	break;
    case 5:		    /* T */
	block[1][1] = 5;
	block[0][0] = 5;
	block[0][1] = 5;
	block[0][2] = 5;
	break;
    case 6:		    /* S */
	block[0][2] = 6;
	block[0][1] = 6;
	block[1][1] = 6;
	block[1][0] = 6;
	break;
    case 7:		    /* Z */
	block[0][1] = 7;
	block[0][0] = 7;
	block[1][1] = 7;
	block[1][2] = 7;
	break;
    default:
	assert(0);
    }
    rotateBlock(block_rot);
    posx = (int) (FIELD_W - blockW) / 2;
} /* 1}}} */

/* void moveBlockDown(void) {{{1 */
void
moveBlockDown(void)
{
    if (paused == 0) {
	if (posy > 0) {
	    int moveTest = 1;

	    for (int y = 0; y < blockH; y++) {
		for (int x = 0; x < blockW; x++) {
		    if ((block[x][y] != 0)) {
			if (field[posx+x][posy+y-1] != 0) {
			    moveTest = 0;
			    break;
			}
		    }
		}
	    }

	    if (moveTest != 0) {
		posy--;
		return;
	    }
	}

	fixBlock();
    }
} /* 1}}} */

/* void fixBlock(void) {{{1 */
void
fixBlock(void)
{
    /* fix block position */
    for (int y = 0; y < blockH; y++) {
	for (int x = 0; x < blockW; x++) {
	    field[posx+x][posy+y] |= block[x][y];
	}
    }

    /* check for compleate flours */
    for (int y = posy; y < FIELD_H; y++) {
	int empty_line = 1;
	for (int x = 0; x < FIELD_W; x++) {
	    if (field[x][y] != 0) {
		empty_line = 0;
		break;
	    }
	}
	if (empty_line != 0) {
	    htop = y - 1;
	    break;
	}
    }
    checkHouse();
    createBlock();
} /* 1}}} */

/* void checkHouse(void) {{{1 */
void
checkHouse(void)
{
    int add_score = 100;
    int tmp_score = 0;
    for (int y = 0; y <= htop; y++) {
	for (int x = 0; x < FIELD_W; x++) {
	    if (field[x][y] == 0) {
		add_score = 0;
		break;
	    }
	}
	if (add_score != 0) {
	    tmp_score = tmp_score * 2 + add_score;
	    clearFlour(&y);
	}
	add_score = 100;
    }
    score += tmp_score;

} /* 1}}} */

/* void clearFlour(int *flour) {{{1 */
void
clearFlour(int *flour)
{
    for (int x = 0; x < FIELD_W; x++) {
//        for (int y = *flour; y<htop; y++) {
        for (int y = *flour; y < FIELD_H - 1; y++) {
	    field[x][y] = field[x][y+1];
	}
    }
    htop--;
    (*flour)--;
    timeout--;
} /* 1}}} */

/* void display(void) {{{1 */
void
display(void)
{
    for (int y = 0; y < blockH; y++) {
	for (int x = 0; x < blockW; x++) {
	    field[posx+x][posy+y] |= block[x][y];
	}
    }

    printf("\x1B[1A\x1B[K\x1B[0,0H╔════════════════════╗\n");
    for (int y = FIELD_H - BLOCK_H - 1; y >= 0; y--) {
	printf("║");
	for (int x = 0; x < FIELD_W; x++) {
	    if (field[x][y] > 0) {
		printf("\x1B[4%dm%X \x1B[49m", field[x][y], field[x][y]);
	    } else
		printf("  ");
	}
	printf("║ %d\n", y);
    }
    printf ("╚════════════════════╝\n");
    msg_debug("posx:%d posy:%d w:%d h:%d htop:%d timeout:%d  score:%d\n", posx,posy,blockW,blockH,htop,timeout,score);

    for (int y = 0; y < blockH; y++) {
	for (int x = 0; x < blockW; x++) {
	    field[posx+x][posy+y] ^= block[x][y];
	}
    }
} /* 1}}} */

/* void displayGL(void) {{{1 */
void displayGL(void)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT);

    for (int y = 0; y < blockH; y++) {
	for (int x = 0; x < blockW; x++) {
	    field[posx+x][posy+y] |= block[x][y];
	}
    }

    for (int y = FIELD_H - BLOCK_H - 1; y >= 0; y--) {
	for (int x = 0; x < FIELD_W; x++) {
	    if (field[x][y] > 0) {
		glWindowPos2i(x*img->width, y*img->height);
		glDrawPixels(img->width, img->height, GL_RGBA, GL_UNSIGNED_BYTE, img->data);
	    }
	}
    }

    for (int y = 0; y < blockH; y++) {
	for (int x = 0; x < blockW; x++) {
	    field[posx+x][posy+y] ^= block[x][y];
	}
    }

    glutSwapBuffers();

} /* 1}}} */

/* void displayResizeGL(GLint screen_width, GLint screen_height) {{{1 */
void
displayResizeGL(GLint screen_width, GLint screen_height)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glShadeModel(GL_SMOOTH);
    glViewport(0, 0, screen_width, screen_height);
    glMatrixMode(GL_PROJECTION);

    gluPerspective(45.0, aspect_ratio, 0.1, 1000.0);
    glutPostRedisplay();
} /* 1}}} */

/* void kbd_funkc(unsigned char key, int x, int y) {{{1 */
void
kbd_funkc(unsigned char key, int x, int y)
{
    if (paused == 0) {
	switch (key) {
	    case ' ':
		rotateBlock(1);
		
		break;

	    case 'p':
	    case 'P':
		msg_debug("paused\n");
		paused = 1;
		break;
	    case 'q':
	    case 'Q':
		exit(0);
	}
	displayGL();
    } else {
	switch (key) {
	    case 'p':
	    case 'P':
		paused = 0;
		break;
	}
    }
} /* 1}}} */

/* void kbd_specfunkc(unsigned char key, int x, int y) {{{1 */
void
kbd_specfunkc(unsigned char key, int x, int y)
{
    if (paused == 0) {
	int test = 0;
	switch (key) {
	case GLUT_KEY_LEFT:
	    if (posx > 0) {
		for (x = 0; x < blockW; x++) {
		    for (y = 0; y < blockH; y++) {
			if (field[posx+x-1][posy+y] > 0 && block[x][y] > 0) {
			    test = 1;
			    break;
			}
		    }
		    if (test != 0) break;
		}
		if (test == 0) posx--;
	    }
	    break;
	    
	case GLUT_KEY_RIGHT:
	    if (posx < FIELD_W - blockW) {
		for (x = 0; x < blockW; x++) {
		    for (y = 0; y < blockH; y++) {
			if (field[posx+x+1][posy+y] > 0 && block[x][y] > 0) {
			    test = 1;
			    break;
			}
		    }
		    if (test != 0) break;
		}
		if (test == 0) posx++;
	    }
	    break;

    //    default:
	}
	displayGL();
    }
} /* 1}}} */

/* vim: set ts=8 sw=4 sts=4 foldminlines=1: */
