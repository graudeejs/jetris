PROG=		jetris

SRCS=		tetris.c msg.c
CSTD=		c99
CFLAGS=		-Wall -pedantic -I/usr/local/include
CFLAGS+=	-g
LDADD=		-L/usr/local/lib -lc -lglut -lGLU -lpng

NOMAN=		1
NO_MAN=		1

install:

cleanall: clean
	rm -f *.core

.include <bsd.prog.mk>
