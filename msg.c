/*-
 * Copyright (c) 2010, Aldis Berjoza <aldis@bsdroot.lv>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following disclaimer
 *    in the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name of the  nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <assert.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

/*
 * Print warning message
 */
void
msg_warn(const char *restrict format,...)
{
	va_list vl;

	va_start(vl, format);
	fprintf(stderr, "WARN: ");
	vfprintf(stderr, format, vl);
	va_end(vl);
}

/*
 * Print error message
 */
void
msg_err(const char *restrict format,...)
{
	va_list vl;

	va_start(vl, format);
	fprintf(stderr, "ERR: ");
	vfprintf(stderr, format, vl);
	va_end(vl);
}

/*
 * Print error message and exit with EXIT_FAILURE
 */
void
msg_die(const char *restrict format,...)
{
	va_list vl;

	va_start(vl, format);
	fprintf(stderr, "ERR: ");
	vfprintf(stderr, format, vl);
	va_end(vl);
#ifndef DEBUG
	exit(EXIT_FAILURE);
#endif
}

/*
 * Print info message
 */
void
msg_info(const char *restrict format,...)
{
	va_list vl;

	va_start(vl, format);
	fprintf(stderr, "INFO: ");
	vfprintf(stderr, format, vl);
	va_end(vl);
}

/*
 * Print debug message
 */
void
msg_debug(const char *restrict format,...)
{
	va_list vl;

	va_start(vl, format);
	fprintf(stderr, "DEBUG: ");
	vfprintf(stderr, format, vl);
	va_end(vl);
}

/* vim: set ts=5 sw=4: */
